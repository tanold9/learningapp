import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-help-create',
  templateUrl: './help-create.component.html',
  styleUrls: ['./help-create.component.css'],
})
export class HelpCreateComponent implements OnInit {
  loading: boolean;
  action: string;
  title: string;
  formGroup: FormGroup;
  category: any;
  itemCategory: any;

  constructor(
    private dialogRef: MatDialogRef<HelpCreateComponent>,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) private data
  ) {
    this.loading = false;
    this.action = data.action;
    this.title = data.title;
    if (this.action === 'update') {
      this.category = data.category;
    }
  }

  ngOnInit() {
    this.formGroup = this.initFormGroup();
  }
  initFormGroup(): FormGroup {
    if (this.action === 'create') {
      return this.formBuilder.group({
        code: ['', Validators.required],
        name: ['', Validators.required],
        description: ['', Validators.required],
      });
    } else {
      return this.formBuilder.group({
        id: [this.category.id, Validators.required],
        code: [this.category.code, Validators.required],
        name: [this.category.name, [Validators.required]],
        description: [this.category.description, [Validators.required]],
      });
    }
  }

  save(payload) {
    if (this.action === 'update') {
      this.edit(this.category.id, payload);
    } else {
      this.create(payload);
    }
  }

  create(payload) {}

  edit(id, payload) {}
}
