import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class ApiServiceService {
  constructor(private http: HttpClient) {}

  handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  throwException() {
    console.log('failed');
  }

  allRegions() {
    return this.http.get(`http://madenimis.tamisemi.go.tz/api/admin-regions`);
  }

  allUsers() {
    return this.http.get('https://jsonplaceholder.typicode.com/users');
  }

  allCategories() {
    return this.http.get(`api/get-category`);
  }

  saveCategory(params) {
    return this.http.post(
      `/api/post-category`,
      params
    );
  }

  deleteCategory(id) {
    return this.http.delete(`/api/topic-category` + '/' + id).pipe();
  }
}
