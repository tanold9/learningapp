import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: any;
  constructor(private apiService: ApiServiceService) {}

  ngOnInit() {
    this.GetUsers();
  }
  GetUsers() {
    console.log('object');
    this.apiService
      .allUsers()
      .pipe()
      .subscribe(
        (response) => {
          console.log(response);
          this.users = response;
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
