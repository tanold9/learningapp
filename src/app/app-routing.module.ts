import { HelpCreateComponent } from './help-create/help-create.component';
import { YaliyomoComponent } from './yaliyomo/yaliyomo.component';
import { SignupComponent } from './signup/signup.component';
import { HelpComponent } from './help/help.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClickComponent } from './click/click.component';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'help',
    component: HelpComponent,
  },
  {
    path: 'signup',
    component: SignupComponent,
  },
  {
    path: 'click',
    component: ClickComponent,
  },
  {
    path: 'info',
    component: YaliyomoComponent,
  },
  {
    path: 'info-create',
    component: HelpCreateComponent,
  },
  {
    path: 'users',
    component: UsersComponent,
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
