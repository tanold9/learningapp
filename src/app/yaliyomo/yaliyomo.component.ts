import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-yaliyomo',
  templateUrl: './yaliyomo.component.html',
  styleUrls: ['./yaliyomo.component.css'],
})
export class YaliyomoComponent implements OnInit {
  regions: any;
  constructor(private apiService: ApiServiceService) {}

  ngOnInit() {
    this.GetRegions();
  }
  GetRegions() {
    console.log('object');
    this.apiService
      .allRegions()
      .pipe()
      .subscribe(
        (response) => {
          this.regions = response['data'];
          console.log('Mikoa yote', this.regions);
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
