import { ApiServiceService } from './../api-service.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css'],
})
export class HelpComponent implements OnInit {
  formGroup: FormGroup;
  todaydate = new Date();
  categories: any;
  constructor(
    private formBuilder: FormBuilder,
    private apiservice: ApiServiceService
  ) {}

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      name: [],
      code: [],
      description: [],
    });

    this.GetCategories();
  }

  save(payload) {
    console.log(payload);
    this.apiservice
      .saveCategory(payload)
      .pipe()
      .subscribe(
        (response) => {
          console.log(response);
          this.GetCategories();
        },
        (error) => {
          console.log(error);
        }
      );
  }

  GetCategories() {
    console.log('object');
    this.apiservice
      .allCategories()
      .pipe()
      .subscribe(
        (response) => {
          console.log(response);
          this.categories = response;
        },
        (error) => {
          console.log(error);
        }
      );
  }

  delete(param) {
    console.log(param);
    Swal.fire({
      title: 'Are you sure you want to delete ?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        this.apiservice
          .deleteCategory(param.id)
          .pipe()
          .subscribe(
            (response) => {
              console.log(response);
              if (response['status'] == 201) {
                Swal.fire('Category Deleted', 'Successfuly', 'success');
                this.GetCategories();
              }
            },
            (error) => {
              console.log(error);
              if (error.status == 400) {
                Swal.fire('Process Failed', error.error.message, 'error');
              }
            }
          );
      }
    });
  }

  edit(param) {
    console.log(param);
  }
}
